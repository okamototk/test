FROM library/nginx:1.15.8

RUN apt-get update \
 && apt-get install -y libcap2-bin \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN chown www-data.www-data -R /var /run

RUN  setcap    cap_net_bind_service=+ep /usr/sbin/nginx \
  && setcap -v cap_net_bind_service=+ep /usr/sbin/nginx

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]

